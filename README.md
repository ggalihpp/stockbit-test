# Guidelines

## RUN

To run the service + gateway + network discovery (etcd), you need to run it using docker-compose
```
docker-compose up
```

## ROUTES

| Endpoint               | Method | Description                                                        | Parameter                                        | cURL Example                                                                                                                                                                                                                                                                                                              |
| ---------------------- | ------ | ------------------------------------------------------------------ | ------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| http://localhost:8801/search             | POST    | Search movies                                            | JSON Body: {"searchword": STRING, "pagination": INT } | curl --request POST --url http://localhost:8801/search-movies --header 'content-type: application/json' --data '{ "searchword": "love","pagination": 2 }'     

## Development

Here's what you need:
- Go 1.11x or later to use go modules (https://github.com/golang/go/wiki/Modules)
- Go-kit (https://github.com/go-kit/kit)
- Go-Kit CLI (https://github.com/kujtimiihoxha/kit)
- Protocol Buffer Compiler (https://developers.google.com/protocol-buffers)
                                                                                                                                                                                                                                                           