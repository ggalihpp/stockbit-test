module gitlab.com/ggalihpp/stockbit-test/gateway

go 1.14

require (
	github.com/go-kit/kit v0.10.0
	github.com/golang/protobuf v1.4.0
	github.com/lightstep/lightstep-tracer-go v0.20.0
	github.com/oklog/oklog v0.3.2
	github.com/opentracing/opentracing-go v1.1.0
	github.com/prometheus/client_golang v1.6.0
	google.golang.org/grpc v1.29.1
	google.golang.org/protobuf v1.21.0
	sourcegraph.com/sourcegraph/appdash v0.0.0-20190731080439-ebfcffb1b5c0
)
