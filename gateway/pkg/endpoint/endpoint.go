package endpoint

import (
	"context"

	endpoint "github.com/go-kit/kit/endpoint"
	"gitlab.com/ggalihpp/stockbit-test/gateway/pb"
	service "gitlab.com/ggalihpp/stockbit-test/gateway/pkg/service"
)

// SearchMoviesRequest collects the request parameters for the SearchMovies method.
type SearchMoviesRequest struct {
	Keyword string `json:"searchword"`
	Page    int    `json:"pagination"`
}

// SearchMoviesResponse collects the response parameters for the SearchMovies method.
type SearchMoviesResponse struct {
	Re    []*pb.Movie `json:"result"`
	Total int32       `json:"total_result"`
	Page  int         `json:"current_page"`
	Err   error       `json:"err"`
}

// MakeSearchMoviesEndpoint returns an endpoint that invokes SearchMovies on the service.
func MakeSearchMoviesEndpoint(s service.GatewayService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(SearchMoviesRequest)
		re, total, currentPage, err := s.SearchMovies(ctx, req.Keyword, req.Page)
		return SearchMoviesResponse{
			Total: total,
			Page:  currentPage,
			Err:   err,
			Re:    re,
		}, nil
	}
}

// Failed implements Failer.
func (r SearchMoviesResponse) Failed() error {
	return r.Err
}

// Failure is an interface that should be implemented by response types.
// Response encoders can check if responses are Failer, and if so they've
// failed, and if so encode them using a separate write path based on the error.
type Failure interface {
	Failed() error
}

// SearchMovies implements Service. Primarily useful in a client.
func (e Endpoints) SearchMovies(ctx context.Context, keyword string, page int) (re []*pb.Movie, err error) {
	request := SearchMoviesRequest{
		Keyword: keyword,
		Page:    page,
	}
	response, err := e.SearchMoviesEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(SearchMoviesResponse).Re, response.(SearchMoviesResponse).Err
}
