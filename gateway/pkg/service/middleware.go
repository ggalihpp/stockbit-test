package service

import (
	"context"

	log "github.com/go-kit/kit/log"
	"gitlab.com/ggalihpp/stockbit-test/gateway/pb"
)

// Middleware describes a service middleware.
type Middleware func(GatewayService) GatewayService

type loggingMiddleware struct {
	logger log.Logger
	next   GatewayService
}

// LoggingMiddleware takes a logger as a dependency
// and returns a GatewayService Middleware.
func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next GatewayService) GatewayService {
		return &loggingMiddleware{logger, next}
	}

}

func (l loggingMiddleware) SearchMovies(ctx context.Context, keyword string, page int) (result []*pb.Movie, totalResult int32, currentPage int, err error) {
	defer func() {
		l.logger.Log("method", "SearchMovies", "keyword", keyword, "page", page, "re", result, "total", totalResult, "err", err)
	}()
	return l.next.SearchMovies(ctx, keyword, page)
}
