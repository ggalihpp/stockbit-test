package service

import (
	"context"
	"fmt"
	"log"

	sdetcd "github.com/go-kit/kit/sd/etcd"
	"gitlab.com/ggalihpp/stockbit-test/gateway/pb"

	"google.golang.org/grpc"
)

// GatewayService describes the service.
type GatewayService interface {
	// Add your methods here
	// e.x: Foo(ctx context.Context,s string)(rs string, err error)
	SearchMovies(ctx context.Context, keyword string, page int) (result []*pb.Movie, totalResult int32, currentPage int, err error)
}

type basicGatewayService struct {
	movieServiceClient pb.MoviesClient
}

func (b *basicGatewayService) SearchMovies(ctx context.Context, keyword string, page int) (result []*pb.Movie, totalResult int32, currentPage int, err error) {
	reply, err := b.movieServiceClient.Search(context.Background(), &pb.SearchRequest{
		Keyword: keyword,
		Page:    int32(page),
	})

	if err != nil {
		return
	}

	fmt.Println(reply.GetMovies())
	fmt.Println("TOTAL RESULT::: ", reply.GetTotalResults())

	return reply.GetMovies(), reply.GetTotalResults(), page, nil
}

// NewBasicGatewayService returns a naive, stateless implementation of GatewayService.
func NewBasicGatewayService() GatewayService {
	var (
		etcdServer = "http://etcd:2379"
		prefix     = "/service/movies/"
	)

	client, err := sdetcd.NewClient(context.Background(), []string{etcdServer}, sdetcd.ClientOptions{})
	if err != nil {
		log.Printf("Unable to connect to etcd: %s", err.Error())
		return new(basicGatewayService)
	}

	entries, err := client.GetEntries(prefix)
	if err != nil || len(entries) == 0 {
		log.Printf("Unable to get Entries: %s", err)
		return new(basicGatewayService)
	}

	conn, err := grpc.Dial(entries[0], grpc.WithInsecure())
	if err != nil {
		log.Printf("Unable to connect to movies: %s", err.Error())
		return new(basicGatewayService)
	}

	return &basicGatewayService{
		movieServiceClient: pb.NewMoviesClient(conn),
	}
}

// New returns a GatewayService with all of the expected middleware wired in.
func New(middleware []Middleware) GatewayService {
	var svc GatewayService = NewBasicGatewayService()
	for _, m := range middleware {
		svc = m(svc)
	}
	return svc
}
