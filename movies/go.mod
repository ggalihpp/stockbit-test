module gitlab.com/ggalihpp/stockbit-test/movies

go 1.14

require (
	github.com/go-kit/kit v0.10.0
	github.com/golang/protobuf v1.4.2
	github.com/lightstep/lightstep-tracer-go v0.20.0
	github.com/oklog/oklog v0.3.2
	github.com/opentracing/opentracing-go v1.1.0
	github.com/prometheus/client_golang v1.6.0
	golang.org/x/net v0.0.0-20200520182314-0ba52f642ac2
	google.golang.org/grpc v1.29.1
	google.golang.org/protobuf v1.23.0
	sourcegraph.com/sourcegraph/appdash v0.0.0-20190731080439-ebfcffb1b5c0
)
