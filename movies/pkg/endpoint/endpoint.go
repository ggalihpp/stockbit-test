package endpoint

import (
	"context"

	"gitlab.com/ggalihpp/stockbit-test/movies/pkg/grpc/pb"

	endpoint "github.com/go-kit/kit/endpoint"
	service "gitlab.com/ggalihpp/stockbit-test/movies/pkg/service"
)

// SearchRequest collects the request parameters for the Search method.
type SearchRequest struct {
	Keyword string `json:"keyword"`
	Page    int    `json:"page"`
}

// SearchResponse collects the response parameters for the Search method.
type SearchResponse struct {
	Movies      []*pb.Movie `json:"movies"`
	TotalResult int32       `json:"total_result"`
	E0          error       `json:"e0"`
}

// MakeSearchEndpoint returns an endpoint that invokes Search on the service.
func MakeSearchEndpoint(s service.MoviesService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(SearchRequest)
		movies, total, e0 := s.Search(ctx, req.Keyword, req.Page)
		return SearchResponse{Movies: movies, TotalResult: int32(total), E0: e0}, nil
	}
}

// Failed implements Failer.
func (r SearchResponse) Failed() error {
	return r.E0
}

// Failure is an interface that should be implemented by response types.
// Response encoders can check if responses are Failer, and if so they've
// failed, and if so encode them using a separate write path based on the error.
type Failure interface {
	Failed() error
}

// Search implements Service. Primarily useful in a client.
func (e Endpoints) Search(ctx context.Context, keyword string, page int) (e0 error) {
	request := SearchRequest{
		Keyword: keyword,
		Page:    page,
	}
	response, err := e.SearchEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(SearchResponse).E0
}
