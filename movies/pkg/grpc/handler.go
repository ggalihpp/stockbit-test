package grpc

import (
	"context"

	grpc "github.com/go-kit/kit/transport/grpc"
	endpoint "gitlab.com/ggalihpp/stockbit-test/movies/pkg/endpoint"
	pb "gitlab.com/ggalihpp/stockbit-test/movies/pkg/grpc/pb"
	context1 "golang.org/x/net/context"
)

// makeSearchHandler creates the handler logic
func makeSearchHandler(endpoints endpoint.Endpoints, options []grpc.ServerOption) grpc.Handler {
	return grpc.NewServer(endpoints.SearchEndpoint, decodeSearchRequest, encodeSearchResponse, options...)
}

// decodeSearchResponse is a transport/grpc.DecodeRequestFunc that converts a
// gRPC request to a user-domain Search request.
// TODO implement the decoder
func decodeSearchRequest(_ context.Context, r interface{}) (interface{}, error) {
	req := r.(*pb.SearchRequest)
	return endpoint.SearchRequest{Keyword: req.Keyword, Page: int(req.Page)}, nil
}

// encodeSearchResponse is a transport/grpc.EncodeResponseFunc that converts
// a user-domain response to a gRPC reply.
// TODO implement the encoder
func encodeSearchResponse(_ context.Context, r interface{}) (interface{}, error) {
	reply := r.(endpoint.SearchResponse)
	return &pb.SearchReply{Movies: reply.Movies, TotalResults: reply.TotalResult}, nil
}

func (g *grpcServer) Search(ctx context1.Context, req *pb.SearchRequest) (*pb.SearchReply, error) {
	_, rep, err := g.search.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	return rep.(*pb.SearchReply), nil
}
