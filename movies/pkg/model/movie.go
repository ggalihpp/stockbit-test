package model

import (
	"gitlab.com/ggalihpp/stockbit-test/movies/pkg/grpc/pb"
)

// MovieSearchResponse -
type MovieSearchResponse struct {
	Search       []*pb.Movie
	TotalResults string `json:"totalResults"`
}
