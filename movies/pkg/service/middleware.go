package service

import (
	"context"

	log "github.com/go-kit/kit/log"
	"gitlab.com/ggalihpp/stockbit-test/movies/pkg/grpc/pb"
)

// Middleware describes a service middleware.
type Middleware func(MoviesService) MoviesService

type loggingMiddleware struct {
	logger log.Logger
	next   MoviesService
}

// LoggingMiddleware takes a logger as a dependency
// and returns a MoviesService Middleware.
func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next MoviesService) MoviesService {
		return &loggingMiddleware{logger, next}
	}

}

func (l loggingMiddleware) Search(ctx context.Context, keyword string, page int) ([]*pb.Movie, int, error) {
	defer func() {
		l.logger.Log("method", "Search", "keyword", keyword, "page", page)
	}()
	return l.next.Search(ctx, keyword, page)
}
