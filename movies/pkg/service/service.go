package service

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/ggalihpp/stockbit-test/movies/pkg/grpc/pb"
	model "gitlab.com/ggalihpp/stockbit-test/movies/pkg/model"
)

// MoviesService describes the service.
type MoviesService interface {
	Search(ctx context.Context, keyword string, page int) ([]*pb.Movie, int, error)
}

type basicMoviesService struct{}

func (b *basicMoviesService) Search(ctx context.Context, keyword string, page int) ([]*pb.Movie, int, error) {
	var result model.MovieSearchResponse

	resp, err := http.Get(fmt.Sprintf("http://www.omdbapi.com/?apikey=faf7e5bb&s=%s&page=%v", keyword, page))
	if err != nil {
		log.Fatalln(err)
		return nil, 0, err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
		return nil, 0, err
	}

	if err = json.Unmarshal(body, &result); err != nil {
		log.Fatalln(err)
		return nil, 0, err
	}

	totalResult, err := strconv.Atoi(result.TotalResults)
	if err != nil {
		log.Fatalln(err)
		return nil, 0, err
	}

	log.Println("RESP BODY:: ", string(body))

	fmt.Println(result)

	return result.Search, totalResult, nil
}

// NewBasicMoviesService returns a naive, stateless implementation of MoviesService.
func NewBasicMoviesService() MoviesService {
	return &basicMoviesService{}
}

// New returns a MoviesService with all of the expected middleware wired in.
func New(middleware []Middleware) MoviesService {
	var svc MoviesService = NewBasicMoviesService()
	for _, m := range middleware {
		svc = m(svc)
	}
	return svc
}
